import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Passenger} from '../models/passenger';
const  HTTP_URL = 'http://localhost:3000/passengers';

@Injectable({
  providedIn: 'root'
})
export class PassengersService {

  constructor(private http: HttpClient) {
  }

  getPassengers(): Observable<Object> {
    console.log('hi get passenger fun')
    return this.http.get(HTTP_URL);
  }

  putPassenger(passenger: Passenger): Observable<Object> {
    console.log('hi putPassenger fun')
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      'myname': 'Kelvin888',
    })

    return this.http.put(HTTP_URL + '/' + passenger.id, passenger, {headers: headers});
  }

  deletePassenger(passenger: Passenger) {
    console.log('hi delete passenger fun')
    return this.http.delete(HTTP_URL + '/' + passenger.id);
  }
  getPassenger(id: number): Observable<object>{
    return this.http.get(`${HTTP_URL}/${id}`);
  }
}
