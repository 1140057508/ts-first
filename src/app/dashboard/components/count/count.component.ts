import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Passenger} from '../../models/passenger';

@Component({
  selector: 'app-count',
  templateUrl: './count.component.html',
  styleUrls: ['./count.component.css']
})
export class CountComponent implements OnInit, OnChanges {

  checkedInNumber: number;
  @Input() items: Passenger[];
  constructor() {
    this.checkedInNumber = 0;
  }

  ngOnInit() {
  }

  //lifecycle hook
  ngOnChanges() {
    if(this.items) {

    this.checkedInNumber = this.items.reduce((num, item) => num += item.checkedIn ? 1 : 0, 0)

    }

  }

}
