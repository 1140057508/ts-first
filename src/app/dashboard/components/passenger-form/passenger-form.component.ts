import {Component, Input, OnInit} from '@angular/core';
import {Luggage, Passenger} from '../../models/passenger';

@Component({
  selector: 'app-passenger-form',
  templateUrl: './passenger-form.component.html',
  styleUrls: ['./passenger-form.component.css']
})
export class PassengerFormComponent implements OnInit {


  @Input() detail: Passenger;
  luggages: Luggage[] = [
    {
      key: 'handbag',
      value: 'Hand Bag',
    },
    {
      key: 'backbag',
      value: 'Back Bag',
    },
    {
      key: 'handonly',
      value: 'Hand Only bag',
    },
    {
      key: 'nonebag',
      value: 'No bag',
    }
  ]
  checkedInDate(){}

  constructor() { }

  ngOnInit() {
  }


  handleCheckedIn(val: boolean){
    if (val) {
      this.detail.checkedInDate = Date.now()
    } else {
      this.detail.checkedInDate = null
    }
  }

handleSelect(val: string) {
   // const newLuggage = this.luggages.filter(
  //    (lug) => lug.key === val
   // )

  // @ts-ignore
 // this.detail.luggage = newLuggage.length > 0 ? newLuggage : [];

  if (val !== this.detail.luggage){
    this.detail.luggage = val
  }
    console.log(newLuggage)
  }
}
}
