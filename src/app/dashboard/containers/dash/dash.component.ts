import { Component, OnInit } from '@angular/core';
import { Passenger } from '../../models/passenger';
import {PassengersService} from '../../services/passengers.service';
import {stringify} from 'querystring';
import {errorObject} from 'rxjs/internal-compatibility';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';

const g_errorFirstapp =' error_firstapp';

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements OnInit {

  errors: boolean;
  errorText: string;
  passengers: Passenger[];
  constructor(private passengerService: PassengersService) {
    this.errors = false;
    this.errorText = '';
    this.passengerService.getPassengers().subscribe(obj => this.passengers = <Passenger[]>obj)
  }

  ngOnInit() {
    let errors = localStorage.getItem('error_firstapp')
    if(errors) {
    //  console.log(errors);

    }
  }

  handleRemove(event) {
    console.log('receiving event: ', event)
    this.passengerService.deletePassenger(event).subscribe( () => {
      this.passengers = this.passengers.filter(p => p.id !== event.id)
    });

  }


  private handleError(error: HttpErrorResponse) {
    console.log('in error...')
    this.errors = true;
    //log errors
    let errors = localStorage.getItem('error_firstapp')
    if(errors) {
      errors += '%|$%' + stringify(error)
    }
    localStorage.setItem('error_firstapp', errors)

    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      //console.error('An error occurred:', error.error.message);
      this.errorText = 'Poor connection, please try again later :('
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      this.errorText = 'Sorry, our server cannot deal with your request currently :('
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };

  handleEdit(event: Passenger) {
    this.passengerService.putPassenger(event).subscribe((obj: Passenger) => {
      this.passengers = this.passengers.map((p) => {
        if(p.id === obj.id) {
          p = {...p, ...obj}
        }
        return p;
      })
    },
        error => {console.log(error);
        this.handleError(error)
      //tell users status
    });
  }
}
