import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {Passenger} from '../../models/passenger';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, OnChanges {

  editting: boolean = false;
  @Input() detail: Passenger;
  @Output() remove: EventEmitter<Passenger> = new EventEmitter();
  @Output() edit: EventEmitter<Passenger> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(change) {
    console.log(change)
    if(change.detail)  {
      this.detail = {...change.detail.currentValue}
    }
  }

  updateName(newName) {
    this.detail.name = newName
  }

  toggleEdit() {
    this.editting = !this.editting;
    if(!this.editting) {
      this.edit.emit(this.detail);
    }
  }
// npm install -g json-server

  removePassenger() {
    this.remove.emit(this.detail);
  }

}
