import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashComponent } from './containers/dash/dash.component';
import { DetailComponent } from './containers/detail/detail.component';
import { CountComponent } from './components/count/count.component';
import {HttpClientModule} from '@angular/common/http';
import { PassengerFormComponent } from './components/passenger-form/passenger-form.component';
import { DashboardViewerComponent } from './containers/dashboard-viewer/dashboard-viewer.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  declarations: [DashComponent, DetailComponent, CountComponent, PassengerFormComponent, DashboardViewerComponent],
  exports: [DashComponent],
})
export class DashboardModule { }
