 interface Child {
  name: string;
  age: number;
}

export interface Luggage {
  key: string;
  value: string;

}

export interface Passenger {
  id: number;
  name: string;
  age: number;
  checkedIn: boolean;
  checkedInDate: number;
  luggage: Luggage;
}
